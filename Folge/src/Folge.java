
public class Folge {

	public static void main(String[] args) {
		
		//99, 96, 93, ... 12, 9
//		for(int i = 99; i >= 9; i-=3) {
//			System.out.println(i);
//		}
		
		//1, 4, 9, 16, 25, ... 361, 400
//		int zahl = 1, erg = 0;
//		while( erg < 400) {
//			erg = erg + zahl;
//			zahl = zahl + 2;
//			System.out.printf("%d, " , erg);
//		}
		
		//2, 6, 10, 14, .. 98, 102
//		for(int i = 2; i <= 102; i+=4) {
//			System.out.println(i);
//		}
			
//		2, 4, 8, 16, 32, ..., 16384, 32768
//		for(int i = 2; i <= 32768; i*=2) {
//			System.out.println(i);
//		}
		
		//4, 16, 36, 64, 100 ... 900, 1024
		int zahl = 2, erg = 0;
		while (erg < 1024) {
			erg = zahl * zahl;
			zahl+=2;
			System.out.printf("%d, " , erg);
			
			
		}
	}
		

}
