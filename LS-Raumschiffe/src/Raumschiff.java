import java.util.ArrayList;

/**
 * Diese Klasse modelliert ein Raumschiff
 * @author Norwin Popiela
 *
 */



public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schilde;
	private int huelle;
	private int lebenserhaltungssysteme;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private String name;
	
	public Raumschiff () {
		schiffsname = "Raumschiff1";
		energieversorgungInProzent = 100;
		schilde = 100;
		huelle = 100;
		lebenserhaltungssysteme = 100;
		androidenAnzahl = 5;
		photonentorpedoAnzahl = 5;
	}
	
	/**
	 * Konstruktor fuer die Klasse Raumschiff
	 * @param photonentorpedoAnzahl
	 * @param energieversorgung des Raumschiffes
	 * @param schilde in Prozent des Raumschiffes
	 * @param huelle in Prozent des Raumschiffes
	 * @param lebenserhaltungssysteme des Raumschiffes
	 * @param androidenAnzahl 
	 * @param schiffsname des Raumschiffes
	 */
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgung, int schilde,int huelle, 
						int lebenserhaltungssysteme, int androidenAnzahl, String schiffsname) {
		
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgung;
		this.schilde = schilde;
		this.huelle = huelle;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	public void zustandAusgeben() {
		System.out.println("Die Werte des Raumschiffes sind: \r\n Energieversorgung: " + this.energieversorgungInProzent + 
				"% Schutzschild: " + this.schilde + "% Hülle: " + this.huelle + " % Lebenserhaltungssysteme: " 
				+ this.lebenserhaltungssysteme + "Die Anzahl der Androiden beträgt: " + this.androidenAnzahl + 
				"Die Anzahl der Photonentorpedos beträgt: " + this.photonentorpedoAnzahl);
	}
	
	public void ladungsAusgabe() {
		System.out.println("Im Lager befinden sich momentan: ");
		for(Ladung i : ladungsverzeichnis) {
			System.out.println(i.getBezeichnung() + "x" + i.getMenge());
		}
	}

	public void photonentorpedoAbschießen(Raumschiff r) {
		if(photonentorpedoAnzahl <= 0) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			this.photonentorpedoAnzahl--;
			treffer(r);
		}
	}
	
	public void phaserkanonenAbschießen(Raumschiff r) {
		if(energieversorgungInProzent <= 50) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			nachrichtAnAlle("Phaserkanone abgeschossen");
			this.energieversorgungInProzent = 50;
			treffer(r);
		}
	}
	
	private void treffer(Raumschiff r) {
		System.out.println(r.name + "wurde getroffen!");
	}
	
	public void nachrichtAnAlle(String nachricht) {
		broadcastKommunikator.add(nachricht);
		System.out.println(nachricht);
	}
	
	/**
	 * Liefert die Anzahl der Photonentorpedos
	 * @return gibt die Anzahl der Photonentorpedos zurück
	 */
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schilde;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schilde = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelle;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelle = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssysteme;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssysteme = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	
	
	
}
