import java.util.Scanner;

class Addition {

    //static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) { //void hei�t das die main Methode nie einen R�ckgabewert hat

        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
        
        programmhinweis();
        zahl1 = eingabe("1. Zahl :");
        zahl2 = eingabe("2. Zahl :");
        erg = verarbeitung(zahl1, zahl2);
        ausgabe(erg, zahl1, zahl2);
        

        //1.Programmhinweis
        //System.out.println("Hinweis: ");
        //System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");

        //4.Eingabe
        //System.out.println(" 1. Zahl:");
        //zahl1 = sc.nextDouble();
        //System.out.println(" 2. Zahl:");
        //zahl2 = sc.nextDouble();

        //3.Verarbeitung
        //erg = zahl1 + zahl2;

        //2.Ausgabe
        //System.out.println("Ergebnis der Addition");
        //System.out.printf("%.2f = %.2f+%.2f", erg, zahl1, zahl2);
    }
    
    public static void programmhinweis () {	//(static = statisch) //Signatur
    	System.out.println("Hinweis");
    	System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
    }
    
    public static void ausgabe (double erg, double zahl1, double zahl2) {
    	System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f = %.2f+%.2f", erg, zahl1, zahl2); //%.2f rundet das Ergebnis auf zwei Nachkommastellen
    }
    
    public static double verarbeitung (double zahl1, double zahl2) {
    	double ergebnis; //Datentyp muss erst deklariert werden, Sie ist nur in der geschweiften Klammer(Methodenrumpf) g�ltig
    	ergebnis = zahl1+zahl2;
    			return ergebnis;
    }
    
    public static double eingabe (String text) {
    	Scanner sc = new Scanner(System.in);
    	System.out.println(text);
    	double zahl = sc.nextDouble();
    	return zahl; //Alles muss in der main Methode noch aufgerufen werden
    	
    }

}




