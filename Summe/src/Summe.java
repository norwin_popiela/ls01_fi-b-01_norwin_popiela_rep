
public class Summe {

	public static void main(String[] args) {
		
		int summe = 0, zahl = 1;
		
		while (zahl <= 50) {
			summe = summe + zahl;
			zahl++; //zahl = zahl + 1;			
			
		}
		
		System.out.println(zahl);
		System.out.println(summe);
		
		summe = 0;
		zahl = 1;
		
		do {
			summe = summe + zahl;
			zahl++; //zahl = zahl + 1;
		}while(zahl <= 50);
		
		System.out.println("\n" + zahl);
		System.out.println(summe);
		
		for(int i = 1; i <= 50; i++) {
			summe = summe + i;
		}
		
		//System.out.println("\n" + i); nicht zul�ssig da i nicht au�erhalb der Z�hlschleife g�ltig ist
		System.out.println("\n" + summe);
	}

}
