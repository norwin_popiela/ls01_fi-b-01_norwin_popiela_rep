﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    while (true) {
    	
    	double gesamtpreis =fahrkartenbestellungErfassen ();
        double eingezahlterGesamtbetrag =fahrkartenBezahlen(gesamtpreis);
        fahrkartenAusgeben ();
        rueckgeldAusgeben (eingezahlterGesamtbetrag, gesamtpreis);
    }
       
       
              
    }
    public static double fahrkartenbestellungErfassen () {
    	Scanner mytastatur = new Scanner  (System.in);
    	System.out.println("Wieviele Tickets für Berlin AB möchten Sie kaufen?");
    	int anzahlTickets = mytastatur.nextInt();
    	if (anzahlTickets < 1 | anzahlTickets > 10) {
    		System.out.println("Sie haben eine ungültige Anzahl an Tickets eingegeben. Die Anzahl der Tickets wird auf 1 gesetzt!");
    		anzahlTickets = 1;
    		
    	}
    	
    	// Ich habe die switch-case Verzweigung verwendet sodass der Benutzer zwischen verschiedenen Ticketarten
    	// wählen kann. Die Preise für die einzelnen Tickets werden berechnet und ausgegeben!
    	System.out.printf("Bitte wählen Sie eine der zur Verfügung stehenden Ticketarten:  \n1. Einzelfahrschein Regeltarif AB [2,90 EUR]" +  "\n2. Tageskarte Regeltarif AB [8,60 EUR]" + "\n3. Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]\n");
    		
    		double Gesamtpreis = 0.0;
    		
        	double einzelfahrschein = 2.90;
        	double tageskarte = 8.60;
        	double kleingruppe = 23.50;
        	int nummerdesTickets = ' ';
        	
        	int artdesTickets = 0;
          	while (artdesTickets < 1 | artdesTickets > 3) {
          		artdesTickets = mytastatur.nextInt();
          	}
        		
            	switch(artdesTickets) {
            	case 1 :
            		Gesamtpreis = anzahlTickets * einzelfahrschein;
            		System.out.printf("Der Preis beträgt: " + Gesamtpreis + " EURO" + "\n");
            		break;
            		
            	case 2 :
            		Gesamtpreis = anzahlTickets * tageskarte;
            		System.out.printf("Der Preis beträgt: " + Gesamtpreis + " EURO" +"\n");
            		break;
            		
            	case 3 :
            		Gesamtpreis = anzahlTickets * kleingruppe;
            		System.out.printf("Der Preis beträgt: " + Gesamtpreis + " EURO" + "\n");
            		break;
            		
            		default:
            			System.out.println("Sie haben eine ungültige Nummer eingegeben!");
            	}
        
        	return Gesamtpreis;	
		}
    

 
    public static double fahrkartenBezahlen (double Gesamtpreis) {
    	Scanner mytastatur = new Scanner (System.in);
    	int eingezahlterGesamtbetrag = 0;
    	double eingeworfeneMünze;
   	 	System.out.println("Bitte werfen Sie mindestens 5.Ct bis 100 Euro ein");
   	 	while(eingezahlterGesamtbetrag < Gesamtpreis) {
   	 	System.out.printf("Noch zu zahlen: %.2f\n", Gesamtpreis - eingezahlterGesamtbetrag);
   	 	eingeworfeneMünze = mytastatur.nextDouble();
   	 	eingezahlterGesamtbetrag += eingeworfeneMünze;
   	 	}
   	 return eingezahlterGesamtbetrag;
   	 
    }
    
     public static void fahrkartenAusgeben () {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben (double eingezahlterGesamtbetrag, double gesamtpreis) {
    	double rückgabebetrag ;
    	rückgabebetrag = eingezahlterGesamtbetrag - gesamtpreis;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen/Scheinen ausgezahlt:");
     	   
//		Ich habe noch ein paar Zeilen hinzugefügt sodass man auch mehr als 2 EURO einwerfen kann!
     	   
     	  while(rückgabebetrag >= 100.0) // 50 EURO-Scheine
          {
       	  System.out.println("100 EURO");
	          rückgabebetrag -= 100.0;
          }
     	  while(rückgabebetrag >= 50.0) // 50 EURO-Scheine
          {
       	  System.out.println("50 EURO");
	          rückgabebetrag -= 50.0;
          }
     	  while(rückgabebetrag >= 20.0) // 20 EURO-Scheine
          {
       	  System.out.println("20 EURO");
	          rückgabebetrag -= 20.0;
          }
     	  while(rückgabebetrag >= 10.0) // 10 EURO-Scheine
          {
       	  System.out.println("10 EURO");
	          rückgabebetrag -= 10.0;
          }
     	  while(rückgabebetrag >= 5.0) // 5 EURO-Scheine
          {
       	  System.out.println("5 EURO");
	          rückgabebetrag -= 5.0;
          }
            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
     
    }
    
    
    	
    
}