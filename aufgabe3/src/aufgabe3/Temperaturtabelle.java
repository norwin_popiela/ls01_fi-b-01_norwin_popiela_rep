package aufgabe3;

public class Temperaturtabelle {
	
	public static void main(String[] args) {
		//Erstellung des Tabellenkopfes mit Fahrenheit, welches um 5 Zeichen nach links (linksb�ndig) verschoben ist!
		System.out.printf("Fahrenheit %-5s" , " | " + "  Celsius" );
		System.out.printf("\n%23s", "-----------------------");
		
		//Nun lege ich die grobe Formatierung f�r die kommenden Zeilen fest!
		System.out.printf("\n%-11d", -20);
		System.out.printf("%2s", "|" );
		System.out.printf("%10.2f", -28.8889);
		
		//Ich benutze nun diese Formatierung und nutze Sie immer wieder f�r alle kommenden Zeilen!
		System.out.printf("\n%-11d", -10);
		System.out.printf("%2s", "|" );
		System.out.printf("%10.2f", -23.3333);
		
		//Aufgrund des "+" was dazu kommt, f�ge ich eine weitere Zeile hinzu sodass ein Plus vor den positiven Zahlen steht.
		//Ich konnte nicht einfach +20 einf�gen, da sonst ein Fehler auftrat, ich habe das Problem mit dem folgendem Code behoben.
		System.out.printf("\n%-1s", "+");
		System.out.printf("%-10d", 0);
		System.out.printf("%2s", "|" );
		System.out.printf("%10.2f", -17.7778);
		
		System.out.printf("\n%-1s", "+");
		System.out.printf("%-10d", 20);
		System.out.printf("%2s", "|" );
		System.out.printf("%10.2f", -6.6667);
		
		System.out.printf("\n%-1s", "+");
		System.out.printf("%-10d", 30);
		System.out.printf("%2s", "|" );
		System.out.printf("%10.2f", -1.1111);
		
	}
}
