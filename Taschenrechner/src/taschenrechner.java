import java.util.Scanner;
public class taschenrechner {

	public static void main(String[] args) {
		
//		Der Benutzer soll zwei Zahlen in das Programm eingeben, danach soll er entscheiden, ob die Zahlen addiert, 
//		subtrahiert, multipliziert oder dividiert werden, diese Entscheidung soll �ber die Eingabe der folgenden Symbole 
//		get�tigt werden: +, -, *, /
//		Nach der Auswahl soll das Ergebnis der Rechnung ausgegeben werden, bzw. eine Fehlermeldung, 
//		falls eine falsche Auswahl getroffen wurde.

		double zahl1 = 0.0;
		double zahl2 = 0.0;
		double ergebnis = 0.0; 
		char rechenoperator = ' ';
		Scanner eingabe = new Scanner(System.in);
		
		System.out.printf("%s\n%s\n" ,"Dieses Programm ist ein simpler Taschenrechner, bei dem Sie zwei Zahlen und einen Rechenoperator" ,"zur Berechnung des Ergebnisses eintragen!");
		
		System.out.println("Geben Sie die erste Zahl ein : ");
			zahl1 = eingabe.nextDouble();
			
		System.out.println("Geben Sie die zweite Zahl ein : ");
			zahl2 = eingabe.nextDouble();
			
		System.out.println("Bitte geben Sie einen mathematischen Rechenoperator ein : ");
			rechenoperator = eingabe.next().charAt(0);
			
			switch(rechenoperator) {
			case '+' : 
				ergebnis = zahl1 + zahl2;
				System.out.println("Das Ergebnis der Rechenoperation lautet " + ergebnis);
				break;
							
			case '-' : 
				ergebnis = zahl1 - zahl2;
				System.out.println("Das Ergebnis der Rechenoperation lautet " + ergebnis);
				break;
				
			case '*' : 
				ergebnis = zahl1 * zahl2;
				System.out.println("Das Ergebnis der Rechenoperation lautet " + ergebnis);
				break;
				
			case '/' : 
				ergebnis = zahl1/zahl2;
				System.out.println("Das Ergebnis der Rechenoperation lautet " + ergebnis);
				break;
				
				default:
					System.out.println("Dies ist kein zul�ssiges Zeichen!");
					break; 
			}
		
			
	}

}
