import java.util.Scanner;
public class Mittelwert {

   public static void main(String[] args) {
	   
	   
      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m = 0.0;
      
      x = eingabe("Erste Zahl");
      y = eingabe("Zweite Zahl");
      m = verarbeitung(x, y);
      ausgabe(x, y, m);
      
      
      
      
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      //m = (x + y) / 2.0;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      //System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   public static double eingabe (String text) {
	   Scanner sc = new Scanner(System.in);
	   System.out.println(text);
	   double zahl = sc.nextDouble();
	   return zahl;
   }
   
   public static double verarbeitung (double x, double y) {
	   double mittelwert;
	   mittelwert = (x + y) / 2.0;
	   return mittelwert;
   }
   
   public static void ausgabe (double x, double y, double m) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	   
   }
}
